import React from "react";

import TaskMangerItem from "./TaskMangerItem";

function TaskManagerList({ tasks, onStatusChange, onDelete }) {
  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Description</th>
          <th scope="col">Status</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>

      <tbody>
        {tasks.map((task) => {
          return (
            <TaskMangerItem
              task={task}
              key={task.id}
              onStatusChange={onStatusChange}
              onDelete={onDelete}
            />
          );
        })}
      </tbody>
    </table>
  );
}

export default TaskManagerList;
