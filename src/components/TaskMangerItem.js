import React from "react";

function TaskMangerItem({ task, onStatusChange, onDelete }) {
  return (
    <tr>
      <th scope="row">{task.id}</th>
      <td>{task.description}</td>
      <td>{task.status ? "Done" : "Unfinished"}</td>
      <td>
        <button
          className="btn"
          onClick={() => {
            onStatusChange({ ...task, status: !task.status });
          }}
        >
          Status
        </button>
        <button
          className="btn btn-primary"
          onClick={() => {
            onDelete(task);
          }}
        >
          Delete
        </button>
      </td>
    </tr>
  );
}
export default TaskMangerItem;
