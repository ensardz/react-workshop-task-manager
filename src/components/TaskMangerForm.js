import React, { useState } from "react";

function TaskMangerForm({ onSubmit, onChange }) {
  const [description, setDescription] = useState("");

  function handleDescriptionChange(event) {
    setDescription(event.target.value);
  }

  function handleSubmit(event) {
    event.preventDefault();
    if (!description) return;
    onSubmit(description);
    setDescription("");
  }
  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="description">What's new?</label>
        <input
          id="description"
          className="form-control"
          placeholder="Description"
          type="text"
          value={description}
          onChange={handleDescriptionChange}
        ></input>
      </div>
      <button className="btn btn-primary" type="submit" value="Submit">
        Add
      </button>
    </form>
  );
}

export default TaskMangerForm;
