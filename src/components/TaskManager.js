import React, { useState, useEffect } from "react";
import TaskManagerForm from "./TaskMangerForm";
import TaskManagerList from "./TaskManagerList";

import * as taskApi from "../api/taskApi";

function TaskManager() {
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    getTaskList();
  }, []);

  async function getTaskList() {
    const _tasks = await taskApi.getTaskList();
    setTasks(_tasks);
  }

  async function saveTask(task) {
    await taskApi.saveTask(task);
    await getTaskList();
  }

  async function updateTask(task) {
    await taskApi.updateTask(task);
    await getTaskList();
  }

  async function deleteTask(task) {
    await taskApi.deleteTask(task);
    await getTaskList();
  }

  function handleTaskSubmit(description) {
    saveTask({ description });
  }

  return (
    <>
      <h1>Task Manager</h1>
      <br />
      <br />
      <TaskManagerForm onSubmit={handleTaskSubmit} />
      <br />
      <br />
      <TaskManagerList
        tasks={tasks}
        onStatusChange={updateTask}
        onDelete={deleteTask}
      />
    </>
  );
}

export default TaskManager;
