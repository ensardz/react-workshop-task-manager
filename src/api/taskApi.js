const TASKS_URL = "http://localhost:3005/tasks";

export async function getTaskList() {
  const response = await fetch(TASKS_URL);
  const data = await response.json();
  return data;
}

export async function saveTask(task) {
  try {
    const response = await fetch(TASKS_URL, {
      method: "POST",
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(task),
    });
    return response.ok;
  } catch (ex) {
    return false;
  }
}

export async function updateTask(task) {
  try {
    const response = await fetch(`${TASKS_URL}/${task.id}`, {
      method: "PATCH",
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(task),
    });
    return response.ok;
  } catch (ex) {
    return false;
  }
}

export async function deleteTask(task) {
  try {
    const response = await fetch(`${TASKS_URL}/${task.id}`, {
      method: "DELETE",
    });
    return response.ok;
  } catch (ex) {
    return false;
  }
}
